#Makefile <sfxhash>

SOURCE := $(wildcard *.c)
OBJ="sfxhash"

$(OBJ):$(SOURCE)
	gcc -o $@ $(SOURCE)

clean:
	-rm -f $(OBJ)
